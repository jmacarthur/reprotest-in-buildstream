# Example of reprotest inside BuildStream sandboxes

This is an example of using the `reprotest` utility to check reproducibility of builds inside the freedesktop-sdk BuildStream project.

The freedesktop-sdk project can be found at https://gitlab.com/freedesktop-sdk/freedesktop-sdk.

The modified version of freedesktop-sdk, with all the element files necessary to run reprobuild, can be found in the repository:

    https://gitlab.com/jmacarthur/freedesktop-sdk.git

In the branch `jmac/reprotest-2`.

# Trying it out

Either use the original repository and copy in the element files from this repository:

* Clone the freedesktop SDK using the URL above, https://gitlab.com/freedesktop-sdk/freedesktop-sdk.
* Copy all the .bst files in this section to freedesktop-sdk/sdk/elements.
* Modify freedesktop-sdk/sdk/elements/base/nano.bst to use reprotest. 'nano.bst.example' from this directory can be copied over the existing nano.bst if you don't want to change it yourself.

Or, clone the modified repository from https://gitlab.com/jmacarthur/freedesktop-sdk.git. This version may be out of date with respect to the mainline freedesktop-sdk repo.
Then, build freedesktop-sdk in the normal way, which I'll describe next.

Freedesktop-sdk has a two-stage build procedure which is documented in the .gitlab-ci.yml file in the root directory of freedesktop-sdk. This is a basic set of instructions extracted from that:

     export ARCH=x86_64
     cd bootstrap                                 
     bst -o target_arch "${ARCH}" build bootstrap-with-links.bst                                     
     bst -o target_arch "${ARCH}" build bootstrap-platform-with-links.bst                            
     bst -o target_arch "${ARCH}" checkout bootstrap-with-links.bst checkout-sdk                     
     bst -o target_arch "${ARCH}" checkout bootstrap-platform-with-links.bst checkout-platform
     mv checkout-sdk "../sdk/bootstrap-image-${ARCH}"
     mv checkout-platform "../sdk/bootstrap-image-platform-${ARCH}"
     cd ../sdk                   
     bst -o target_arch "${ARCH}" build all.bst

If reprotest has worked correctly, we expect this build to fail at the point of building 'base/nano.bst', since that is tested and found to be non-reproducible.

# Using reprotest in a build-commands section:

To use reprotest, you only need to wrap the existing build commands in a string and pass that as an argument to reprotest. At the moment, we also need to disable some features of reprotest, since freedesktop-sdk does not have them in its bootstrap environment. In particular, we need to disable `diffoscope`, the difference viewing tool, since it has some unknown errors working in the sandbox, and three variations which reprotest can perform - user_group, fileordering and domain_host because these have runtime dependencies not in freedesktop-sdk's bootstrap image. We also need to pass the specification of the things we want to check for differences. At the moment, I'm only checking the `nano` binary, not any other build artifacts.

The following command runs the build "make -C bst_build_dir && make install".

    reprotest --no-diffoscope --store-dir bst_build_dir/reprotest-artifacts --vary "-user_group, -fileordering, -domain_host" "make -C bst_build_dir && make install" bst_build_dir/src/nano

You also need to add reprotest as a build-time dependency of the element you're testing. For example, this is the depends section of 'base/nano.bst':

    depends:
      - filename: bootstrap-import.bst
	type: build
      - filename: base/pkg-config.bst
      - filename: python-reprotest.bst
	type: build

# Effects of using reprotest

When build-commands are wrapped in reprotest, reprotest will run the whole build process for that element twice, varying the environment for each, and report whether or not the output artifacts were bit-for-bit identical.

If they aren't, reprotest returns a nonzero error code - so BuildStream will interpret this as a failed build and stop at that point. If they do match, the artifacts that would normally by created by a single build will be used as the artifacts of the element by BuildStream, so reprotest should have no effect on the build process (other than doubling the build time).

There is no way to automatically add or disable reprotest for particular elements at the moment.

# Changes to reprotest

There have been some changes to reprotest, which is why the reprotest.bst element included here fetches from a fork of the original reprotest. The original version (at the time of writing) assumes Debian if it cannot determine the build environment. The BuildStream sandbox is detected as Debian by reprotest, but doesn't provide all the features one would expect from a base Debian install, so reprotest fails.

* A new SystemInterface called NoPackageInterface, which does not support any package management.
* Replacement of the Python library call getpass.getuser() with something else - there is no valid username in the sandbox, so we use the user ID instead.
* Not calling 'su' to change to the testbed user if we are already userid 0; the BuildStream sandbox does not have 'su', so we just skip it.
* Removal of the calls to tasksel and setarch.

In the above situation, by 'BuildStream sandbox' I mean the one present while building 'nano' for the freedesktop-sdk project. Other projects may have these features at some point in their build process.

# Future work

There are at least two possible ways to continue with this work:

* Add more control over reprotest within BuildStream; ideally adding an option to run `bst build --repro=all finished-system.bst`. This would run reprotest when building all appropriate elements, and could fall back to a normal build if a reproducibility error occurs, so a report of the reproducibility of the full build can be produced. Not all elements can be tested - any dependency of reprotest, such as `python-rstr`, cannot be tested for reproducibility for obvious reasons.

* Integrate the variations reprotest does into BuildStream itself, so BuildStream can set up the environment for the sandbox with varied environments. This may require a lot of work to move the variation code out of reprotest; I haven't inspected reprotest in enough detail to see how modular it is yet. However, if we do this, it allows us to run reproducible build tests with varying environments without altering the built project at all, and to run on the most basic elements of that project without worrying about dependencies.
  It would be desirable to keep our version of the reprotest code up to date with the upstream version. To effect that, we may want to contribute some time to modularising the reprotest code.